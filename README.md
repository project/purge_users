# Auto Purge Users
Auto Purge Users lets administrators delete inactive users based on time
conditions.

## Table of contents

- About
- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## About
Users are selected as per criteria that check for different types of user
inactivity. Some of the criteria that are used to select users are

- Those who exceed a configured period of inactivity in days, months, etc..
- Those who have not activated their account since registration for a specified
period of time.
- Those who have not logged in for a long periods of time.

The users who are purged can be notified that their account has been purged.
Optionally you can limit the purge to specific roles like authenticated, etc..
You can go to “Administer -> People -> Auto Purge Users” to configure the
duration of account inactivity, status of account and filter the users by roles.
The content created by the users being purged can also be purged or can be
re-assigned to anonymous users after the purge. The user cancellation methods on
the form provides the functionality to assign purged user content to anonymous
user or entirely delete the content. Users can be deleted on cron by enabling
the auto purge option in configuration page or can be deleted manually by
pressing the delete users button. Users deleted during cron are logged via the
watchdog.

### Important notes:

This module has to use **custom logic especially when reassigning content to
guest / anonymous**. So please

- test and use the module carefully
- ensure to have regular and working backups
- exclude roles which should never be touched (especially roles with lots of
  content created)
- report and fix bugs and issues with us

The background is, that there are no good core concepts for some of the cases,
which would allow to solve these better.

## Requirements

This module requires no modules outside of Drupal core.

## Recommended modules

[Markdown filter](https://www.drupal.org/project/markdown): When enabled,
display of the project's README.md help will be rendered with markdown.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Go to the "Purge Users" page ('/admin/people/purge-rule') and modify the module
accordingly:

- "Purge users who have never logged in for" will purge users who created a
  account but never logged in.
- "Purge users who have not logged in for" will purge users who have not logged
  in for a set amount of time.
- "Purge users whose account has not been activated for" will purge users who
  have not activated their account for a set amount of time.
- "Purge users who have been blocked for" will purge users who have been blocked
  for a set amount of time.
- "Role limitation" allows the user to include / exclude user roles to limit the
  purge to specific user roles only.
- "When cancelling a user account" defines how the purged users are handled.
- "Disregard inactive/blocked users" ignores inactive and blocked users for the
  purge.
- 'Purge on cron" purges users during a cron run.
- You can also send users messages, noticing them about their upcoming deletion
  and noticing them, that their user got deleted.

## Maintainers

- Jim Applebee - [jim.applebee](https://www.drupal.org/u/jimapplebee)
- Julian Pustkuchen - [Anybody](https://www.drupal.org/u/anybody)
- Sajal Tiwari - [Er.Sajal](https://www.drupal.org/u/ersajal)
