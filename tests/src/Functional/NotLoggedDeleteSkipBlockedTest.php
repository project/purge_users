<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Purge users who did not log in for a specific period.
 *
 * - Purge method: delete the account and make its
 * content belong to the Anonymous user.
 * - Disregard inactive/blocked users selected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class NotLoggedDeleteSkipBlockedTest extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');

    // Set the users for this scenario.
    $this->addAdminUser();
    $this->createTestUser();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_lastlogin_value', '10')
      ->set('user_lastlogin_period', 'month')
      ->set('enabled_loggedin_users', TRUE)
      ->set('purge_user_cancel_method', 'user_cancel_reassign')
      ->set('disregard_blocked_users', TRUE)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user.
   */
  protected function checkTestResults(): void {
    $account = $this->userStorage->load($this->admin->id());
    $this->assertNotNull($this->admin);

    // Never logged user.
    $account = $this->userStorage->load($this->admin->id());
    $this->assertNotNull($this->neverLoggedUser);

    // Active user to be deleted.
    $account = $this->userStorage->load($this->activeUserToDelete->id());
    $this->assertNull($account);

    // Blocked user.
    $account = $this->userStorage->load($this->admin->id());
    $this->assertNotNull($this->blockedUser);
  }

  /**
   * User settings, expected to be deleted.
   */
  protected function createTestUser(): void {
    // User is created 12 months ago and never logged in.
    $this->neverLoggedUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-12 month'),
      'login' => 0,
    ]);

    // User is created 20 months ago and last logged in 13 months ago.
    // Expected to be deleted.
    $this->activeUserToDelete = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-20 month'),
      'login' => strtotime('-13 month'),
    ]);

    // User is created 20 months ago and last logged in 13 months ago.
    // Expected to be skipped due to status 0
    // (option disregard_blocked_users selected).
    $this->blockedUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-20 month'),
      'login' => strtotime('-13 month'),
    ]);
  }

}
