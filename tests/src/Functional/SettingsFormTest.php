<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests submission of Purge Users form.
 *
 * @group purge_users
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['purge_users'];

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;


  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('administrator', 'administrator'));
    $this->adminUser->save();
  }

  /**
   * {@inheritdoc}
   */
  public function testConfigForm(): void {
    $assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->adminUser);

    // Basic form settings.
    $edit = $this->getFormValues();

    // There must be at least two roles,
    // otherwise it won't be possible to select
    // purge_excluded_users_roles.
    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    $this->assertGreaterThanOrEqual(2, count($roles));

    $this->drupalGet('admin/people/purge-rule');
    $assert->statusCodeEquals(200);

    // Submit the form.
    foreach ($edit as $key => $value) {
      $page->fillField($key, $value);
    }
    $page->findButton('Save configuration')->press();
    $assert->statusCodeEquals(200);
    $assert->pageTextContains('The configuration options have been saved.');

    // Verify form data.
    $this->drupalGet('admin/people/purge-rule');
    $assert->statusCodeEquals(200);
    $assert->fieldValueEquals('user_never_lastlogin_value', '20');
    $assert->fieldValueEquals('user_never_lastlogin_period', 'days');
    $assert->fieldValueEquals('enabled_never_loggedin_users', TRUE);
    $assert->fieldValueEquals('user_lastlogin_value', '');
    $assert->fieldValueEquals('user_lastlogin_period', 'days');
    $assert->fieldValueEquals('user_inactive_value', '');
    $assert->fieldValueEquals('user_inactive_period', 'days');
    $assert->fieldValueEquals('user_blocked_value', '');
    $assert->fieldValueEquals('user_blocked_period', 'days');
    $assert->fieldValueEquals('enabled_loggedin_users', FALSE);
    $assert->fieldValueEquals('enabled_inactive_users', FALSE);
    $assert->fieldValueEquals('enabled_blocked_users', FALSE);
    $assert->fieldValueEquals('purge_included_users_roles[authenticated]', 'authenticated');
    $assert->fieldValueEquals('purge_excluded_users_roles[administrator]', 'administrator');
    $assert->fieldValueEquals('purge_on_cron', FALSE);
    $assert->fieldValueEquals('purge_user_cancel_method', 'user_cancel_block');
    $assert->fieldValueEquals('disregard_blocked_users', FALSE);
  }

  /**
   * Returns the array with the form values to submit.
   *
   * @return array
   *   The array to use with formSubmit function.
   */
  protected function getFormValues(): array {
    $edit['purge_excluded_users_roles[administrator]'] = 'administrator';
    $edit['purge_included_users_roles[authenticated]'] = 'authenticated';
    $edit['purge_user_cancel_method'] = 'user_cancel_block';
    $edit['user_never_lastlogin_value'] = '20';
    $edit['user_never_lastlogin_period'] = 'days';
    $edit['enabled_never_loggedin_users'] = TRUE;
    $edit['enabled_inactive_users'] = FALSE;
    $edit['enabled_loggedin_users'] = FALSE;
    $edit['enabled_blocked_users'] = FALSE;
    $edit['send_email_notification'] = FALSE;

    return $edit;
  }

}
