<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Purge users who did not log in for a specific period.
 *
 * - Purge method: disable the account and unpublish its content.
 * - Disregard inactive/blocked users selected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class NotLoggedBlockedUnpublishTest extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');
    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');

    // Set the users for this scenario.
    $this->addAdminUser();
    $this->createTestUser();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_lastlogin_value', '10')
      ->set('user_lastlogin_period', 'month')
      ->set('enabled_loggedin_users', TRUE)
      ->set('purge_user_cancel_method', 'user_cancel_block_unpublish')
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user.
   */
  protected function checkTestResults(): void {
    // Admin user is not blocked.
    $account = $this->userStorage->load($this->admin->id());
    $this->assertFalse($account->isBlocked());

    // Never logged user is not blocked.
    $account = $this->userStorage->load($this->neverLoggedUser->id());
    $this->assertFalse($account->isBlocked());

    // Active user is blocked.
    $account = $this->userStorage->load($this->activeUserToBlock->id());
    $this->assertTrue($account->isBlocked());

    // Confirm user's content is published.
    $test_node = $this->nodeStorage->loadUnchanged($this->node->id());
    $this->assertFalse($test_node->isPublished());

    // Active is not blocked.
    $account = $this->userStorage->load($this->activeUser->id());
    $this->assertFalse($account->isBlocked());
  }

  /**
   * Active user settings.
   *
   * Expected to be blocked
   * and their content unpublished.
   */
  protected function createTestUser(): void {
    // User is created 12 months ago and never logged in.
    $this->neverLoggedUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-12 month'),
      'login' => 0,
    ]);

    $this->activeUserToBlock = $this->createUser();

    $this->node = $this->createNode([
      'uid' => $this->activeUserToBlock->id(),
      'published' => TRUE,
    ]);
    $this->node->save();

    $this->activeUserToBlock->created = strtotime("-20 month");
    $this->activeUserToBlock->login = strtotime("-13 month");
    $this->activeUserToBlock->save();

    // User is created 20 months ago and logged in 3 days ago.
    $this->activeUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-20 month'),
      'login' => strtotime('-3 day'),
    ]);
  }

}
