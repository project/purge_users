<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Base class for purge users testing.
 *
 * @group purge_users
 */
abstract class SettingsBase extends BrowserTestBase {

  use CronRunTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'purge_users'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $admin;

  /**
   * An active user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $activeUser;

  /**
   * Blocked user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $blockedUser;

  /**
   * A blocked user that will be deleted.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $blockedUserToDelete;

  /**
   * A user with login 0.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $neverLoggedUser;

  /**
   * An active user that will be blocked.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $activeUserToDelete;

  /**
   * An active user that will be blocked.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $activeUserToBlock;

  /**
   * A node created by a user.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * Node storage.
   *
   * @var \Drupal\user\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * User storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Add one admin user.
   */
  protected function addAdminUser(): void {
    $this->admin = $this->createUser([
      'administer site configuration',
      'administer permissions',
      'administer account settings',
      'administer users',
    ], NULL, TRUE);
    $this->admin->save();
  }

  /**
   * Set the basic configuration for all tests.
   *
   * Each test will override accordingly.
   */
  protected function setBasicConfig(): void {
    $this->config('purge_users.settings')
      ->set('enabled_inactive_users', FALSE)
      ->set('enabled_loggedin_users', FALSE)
      ->set('enabled_never_loggedin_users', FALSE)
      ->set('enabled_blocked_users', FALSE)
      ->set('purge_excluded_users_roles', ['administrator'])
      ->set('purge_included_users_roles', ['authenticated'])
      ->set('purge_user_cancel_method', 'user_cancel_delete')
      ->set('send_email_notification', FALSE)
      ->set('inactive_user_notify_subject', 'Dear user')
      ->set('inactive_user_notify_text', 'Dear User, Your account has been deleted due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website. Thank you')
      ->set('user_before_deletion_subject', 'Dear user')
      ->set('send_email_user_before_notification', FALSE)
      ->set('user_before_deletion_text', 'Dear User, Your account will be deleted soon due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website. Thank you')
      ->save();
  }

  /**
   * Tests the confirmation form.
   *
   * The query will check the created and login date < chosen period
   * and the conditions login != 0 and status = 0.
   */
  public function testConfirmForm(): void {
    $this->drupalLogin($this->admin);
    $this->drupalGet('admin/people/purge-rule/confirm');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], 'Confirm');
    $this->assertSession()->statusCodeEquals(200);
    $this->checkConfirmFormResults();
  }

  /**
   * Resets the configuration and run cron.
   */
  public function testCron(): void {
    // Rerun the settings.
    $this->config('purge_users.settings')
      ->set('purge_on_cron', TRUE)
      ->save();
    $this->cronRun();
    $this->checkCronResults();
  }

  /**
   * Creates a user with specific settings for each test.
   */
  abstract protected function createTestUser(): void;

  /**
   * Verifies the expected results of each test.
   */
  abstract protected function checkConfirmFormResults(): void;

  /**
   * Verifies the expected results of each test.
   */
  abstract protected function checkCronResults(): void;

  /**
   * Return the structure and content of a deletion notification email.
   *
   * @return array
   *   The email structure and content.
   */
  protected static function getDeletionExpectedEmail(): array {
    return [
      'id' => 'purge_users_delete_users',
      'module' => 'purge_users',
      'key' => 'delete_users',
      'to' => 'testemail@test.test',
      'from' => 'simpletest@example.com',
      'reply-to' => NULL,
      'langcode' => 'en',
      'params' => [
        'subject' => 'Dear user',
        'body' => 'Dear User, Your account has been deleted due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website. Thank you',
      ],
      'send' => TRUE,
      'subject' => 'Dear user',
      'body' => "Dear User, Your account has been deleted due the website’s policy to\nautomatically remove users who match certain criteria. If you have concerns\nregarding the deletion, please talk to the administrator of the website.\nThank you\n",
      'headers' => [
        'MIME-Version' => '1.0',
        'Content-Type' => 'text/plain; charset=UTF-8; format=flowed; delsp=yes',
        'X-Mailer' => 'Drupal',
        'Return-Path' => 'simpletest@example.com',
        'Sender' => 'simpletest@example.com',
        'From' => 'Drupal <simpletest@example.com>',
        'Content-Transfer-Encoding' => '8Bit',
      ],
    ];
  }

  /**
   * Return the structure and content of a cancellation notification email.
   *
   * @return array
   *   The email structure and content.
   */
  protected static function getCancellationExpectedEmail(): array {
    return [
      'id' => 'purge_users_cancel_users',
      'module' => 'purge_users',
      'key' => 'cancel_users',
      'to' => 'testemail@test.test',
      'from' => 'simpletest@example.com',
      'reply-to' => NULL,
      'langcode' => 'en',
      'params' => [
        'subject' => 'Dear user',
        'body' => 'Dear User, Your account will be deleted soon due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website. Thank you',
      ],
      'send' => TRUE,
      'subject' => 'Dear user',
      'body' => "Dear User, Your account will be deleted soon due the website’s policy to\nautomatically remove users who match certain criteria. If you have concerns\nregarding the deletion, please talk to the administrator of the website.\nThank you\n",
      'headers' => [
        'MIME-Version' => '1.0',
        'Content-Type' => 'text/plain; charset=UTF-8; format=flowed; delsp=yes',
        'X-Mailer' => 'Drupal',
        'Return-Path' => 'simpletest@example.com',
        'Sender' => 'simpletest@example.com',
        'From' => 'Drupal <simpletest@example.com>',
        'Content-Transfer-Encoding' => '8Bit',
      ],
    ];
  }

}
