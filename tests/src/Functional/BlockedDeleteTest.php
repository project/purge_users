<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Purge users who have been blocked for a specific period.
 *
 * - Purge method: Delete the account and its content.
 * - Disregard inactive/blocked users unselected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class BlockedDeleteTest extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');
    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');

    // Set the users for this scenario.
    $this->addAdminUser();

    // Blocked user created 150 days ago, never logged in, status = 0.
    $this->blockedUser = $this->createUser();
    $this->blockedUser->created = strtotime("-150 day");
    $this->blockedUser->login = 0;
    $this->blockedUser->status = 0;
    $this->blockedUser->save();

    // Set the user that will be deleted.
    $this->createTestUser();

    // Active user created 20 months ago and logged in 3 days ago.
    $this->activeUser = $this->createUser();
    $this->activeUser->created = strtotime("-20 month");
    $this->activeUser->login = strtotime("-3 day");
    $this->activeUser->save();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_blocked_value', '100')
      ->set('user_blocked_period', 'days')
      ->set('enabled_blocked_users', TRUE)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user.
   */
  protected function checkTestResults(): void {
    $account = $this->userStorage->load($this->admin->id());
    $this->assertNotNull($account);

    // Blocked user is not deleted.
    $account = $this->userStorage->load($this->blockedUser->id());
    $this->assertNotNull($account);

    // Blocked user is deleted.
    $account = $this->userStorage->load($this->blockedUserToDelete->id());
    $this->assertNull($account);

    // Confirm user's content is deleted.
    $test_node = $this->nodeStorage->loadUnchanged($this->node->id());
    $this->assertNull($test_node);

    $account = $this->userStorage->load($this->activeUser->id());
    $this->assertNotNull($account);
  }

  /**
   * Settings for a user to be deleted.
   *
   * This user and their content will be deleted.
   */
  protected function createTestUser(): void {
    $this->blockedUserToDelete = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-150 day'),
      'login' => strtotime('-150 day'),
    ]);

    // Node of this user.
    $this->node = $this->createNode([
      'uid' => $this->blockedUserToDelete->id(),
      'created' => strtotime('-150 day'),
      'changed' => strtotime('-150 day'),
    ]);

    $this->blockedUserToDelete->status = FALSE;
    $this->blockedUserToDelete->save();
  }

}
