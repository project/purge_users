<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Site policy applied.
 *
 * Purge users who have been blocked for a specific period.
 *
 * - Purge method: Follow site's policy.
 * - Disregard inactive/blocked users unselected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class BlockedSitePolicyDeleteTest extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');

    // Set the users for this scenario.
    $this->addAdminUser();
    $this->createTestUser();

    // Set the users config purge method to delete,
    // default on user_cancel_block.
    $this->config('user.settings')
      ->set('cancel_method', 'user_cancel_delete')
      ->save();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_blocked_value', '100')
      ->set('user_blocked_period', 'days')
      ->set('enabled_blocked_users', TRUE)
      ->set('purge_user_cancel_method', 'user_cancel_site_policy')
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user.
   */
  protected function checkTestResults(): void {
    $account = $this->userStorage->load($this->admin->id());
    $this->assertNotNull($account);

    // Blocked user is not deleted.
    $account = $this->userStorage->load($this->blockedUser->id());
    $this->assertNotNull($account);

    // Blocked user is deleted.
    $account = $this->userStorage->load($this->blockedUserToDelete->id());
    $this->assertNull($account);

    $account = $this->userStorage->load($this->activeUser->id());
    $this->assertNotNull($account);
  }

  /**
   * Sets the user to be deleted.
   *
   * This user and their content will be deleted.
   */
  protected function createTestUser(): void {
    // Blocked user is created 150 days ago, never logged in
    // and status = 0.
    $this->blockedUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-150 day'),
      'login' => 0,
    ]);
    $this->blockedUser->status = 0;
    $this->blockedUser->save();

    $this->blockedUserToDelete = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-150 day'),
      'login' => strtotime('-150 day'),
    ]);
    $this->blockedUserToDelete->status = 0;
    $this->blockedUserToDelete->save();

    // Active user is created 20 months ago and logged in 3 days ago.
    $this->activeUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-20 month'),
      'login' => strtotime('-3 day'),
    ]);
  }

}
