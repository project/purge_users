<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Purge users who did not log in for a specific period.
 *
 * - Purge method: delete the account and
 * make its content belong to the Anonymous user.
 * - Disregard inactive/blocked users unselected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class NotLoggedDeleteAnonymizeTest extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');
    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');

    // Set the users for this scenario and create the node:
    $this->addAdminUser();
    $this->createTestUser();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_lastlogin_value', '10')
      ->set('user_lastlogin_period', 'month')
      ->set('enabled_loggedin_users', TRUE)
      ->set('purge_user_cancel_method', 'user_cancel_reassign')
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user and their content.
   */
  protected function checkTestResults(): void {
    $account = $this->userStorage->load($this->admin->id());
    $this->assertNotNull($account);

    // Never logged in user.
    $account = $this->userStorage->load($this->neverLoggedUser->id());
    $this->assertNotNull($account);

    // Active user to be deleted.
    $account = $this->userStorage->load($this->activeUserToDelete->id());
    $this->assertNull($account);

    // Check the users node ownership:
    $test_node = $this->nodeStorage->loadUnchanged($this->node->id());
    $this->assertTrue(($test_node->getOwnerId() == 0 && $test_node->isPublished()));

    // Active user.
    $account = $this->userStorage->load($this->activeUser->id());
    $this->assertNotNull($account);
  }

  /**
   * Active user settings.
   *
   * Expected to be deleted,
   * their content anonymized.
   */
  protected function createTestUser(): void {
    // User is created 12 months ago and never logged in.
    $this->neverLoggedUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-12 month'),
      'login' => 0,
    ]);

    $this->activeUserToDelete = $this->createUser();

    // Create a single node:
    $this->node = $this->createNode([
      'uid' => $this->activeUserToDelete->id(),
      'published' => TRUE,
    ]);

    $this->activeUserToDelete->created = strtotime("-20 month");
    $this->activeUserToDelete->login = strtotime("-13 month");
    $this->activeUserToDelete->save();

    // User is created 20 months ago and logged in 3 days ago.
    $this->activeUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-20 month'),
      'login' => strtotime('-3 day'),
    ]);
  }

}
