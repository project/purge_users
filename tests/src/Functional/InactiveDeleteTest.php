<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Purge users whose account has not been activated for a specific period.
 *
 * - Purge method: Delete the account and its content.
 * - Disregard inactive/blocked users unselected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class InactiveDeleteTest extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');

    // Set the users for this scenario.
    $this->addAdminUser();
    $this->createTestUser();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_inactive_value', '1')
      ->set('user_inactive_period', 'year')
      ->set('enabled_inactive_users', TRUE)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user.
   */
  protected function checkTestResults(): void {
    // Admin account is not deleted.
    $account = $this->userStorage->load($this->admin->id());
    $this->assertNotNull($account);

    // Blocked user.
    $account = $this->userStorage->load($this->blockedUser->id());
    $this->assertNotNull($account);

    // Blocked user to be deleted.
    $account = $this->userStorage->load($this->blockedUserToDelete->id());
    $this->assertNull($account);

    // Active user.
    $account = $this->userStorage->load($this->activeUser->id());
    $this->assertNotNull($account);
  }

  /**
   * Settings for a blocked user expected to be deleted.
   */
  protected function createTestUser(): void {
    // User is created 6 months ago, never logged in
    // and status = 0.
    // Expected not to be blocked.
    $this->blockedUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-6 days'),
      'login' => 0,
    ]);
    $this->blockedUser->status = 0;
    $this->blockedUser->save();

    $this->blockedUserToDelete = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-3 year'),
      'login' => 0,
    ]);
    $this->blockedUserToDelete->status = 0;
    $this->blockedUserToDelete->save();

    // User is created 20 months ago and logged in 3 days ago.
    $this->activeUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('20 month'),
      'login' => strtotime('-3 day'),
    ]);
  }

}
