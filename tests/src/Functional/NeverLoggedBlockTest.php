<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Purge users who have never logged in for a specific period.
 *
 * - Purge method: Disable the account and keep its content.
 * - Disregard inactive/blocked users unselected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class NeverLoggedBlockTest extends SettingsBase {

  /**
   * A user with login 0 that will be blocked.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $neverLoggedUserToBlock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');

    // Set the users for this scenario.
    $this->addAdminUser();
    $this->createTestUser();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_never_lastlogin_value', '20')
      ->set('user_never_lastlogin_period', 'days')
      ->set('enabled_never_loggedin_users', TRUE)
      ->set('purge_user_cancel_method', 'user_cancel_block')
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user.
   */
  protected function checkTestResults(): void {
    // Admin account is not deleted.
    $account = $this->userStorage->load($this->admin->id());
    $this->assertFalse($account->isBlocked());

    // User is blocked.
    $account = $this->userStorage->load($this->neverLoggedUserToBlock->id());
    $this->assertTrue($account->isBlocked());

    // Active user is not blocked.
    $account = $this->userStorage->load($this->activeUser->id());
    $this->assertFalse($account->isBlocked());
  }

  /**
   * Settings for user expected to be blocked.
   */
  protected function createTestUser(): void {
    $this->neverLoggedUserToBlock = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-122 day'),
      'login' => 0,
    ]);

    // User is created 200 days ago and logged in 100 days ago.
    $this->activeUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-200 day'),
      'login' => strtotime('-100 day'),
    ]);
  }

}
