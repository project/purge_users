<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Functional;

/**
 * Purge users who have not logged in for a specific period.
 *
 * - Purge method: disable the account and keep its content.
 * - Disregard inactive/blocked users unselected.
 * - User Deletion Notification unselected.
 *
 * @group purge_users
 */
class NotLoggedBlockTest extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userStorage = $this->container->get('entity_type.manager')->getStorage('user');
    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');

    // Set the users for this scenario.
    $this->addAdminUser();
    $this->createTestUser();

    // Set the basic configuration and add the specific changes.
    $this->setBasicConfig();
    $this->config('purge_users.settings')
      ->set('user_lastlogin_value', '10')
      ->set('user_lastlogin_period', 'month')
      ->set('enabled_loggedin_users', TRUE)
      ->set('purge_user_cancel_method', 'user_cancel_block')
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkConfirmFormResults(): void {
    $this->checkTestResults();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCronResults(): void {
    $this->checkTestResults();
  }

  /**
   * Check the state of each user.
   */
  protected function checkTestResults(): void {
    // Admin user.
    $account = $this->userStorage->load($this->admin->id());
    $this->assertFalse($account->isBlocked());

    // Never logged in user.
    $account = $this->userStorage->load($this->neverLoggedUser->id());
    $this->assertFalse($account->isBlocked());

    // Active user to be blocked.
    $account = $this->userStorage->load($this->activeUserToBlock->id());
    $this->assertTrue($account->isBlocked());

    // Active user content.
    $test_node = $this->nodeStorage->loadUnchanged($this->node->id());
    $this->assertTrue($test_node->isPublished());

    // Active.
    $account = $this->userStorage->load($this->activeUser->id());
    $this->assertFalse($account->isBlocked());
  }

  /**
   * Active user to be blocked.
   *
   * Expected to be blocked,
   * their content still published.
   */
  protected function createTestUser(): void {
    // User is created 12 months ago and never logged in.
    // Expected not to be blocked.
    $this->neverLoggedUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-12 month'),
      'login' => 0,
    ]);

    $this->activeUserToBlock = $this->createUser();

    // The user has also created content that will be kept.
    $this->node = $this->createNode([
      'uid' => $this->activeUserToBlock->id(),
      'published' => TRUE,
    ]);
    $this->node->save();

    $this->activeUserToBlock->created = strtotime("-20 month");
    $this->activeUserToBlock->login = strtotime("-13 month");
    $this->activeUserToBlock->save();

    // User is created 20 months ago and logged in 3 days ago.
    $this->activeUser = $this->createUser([], NULL, FALSE, [
      'created' => strtotime('-20 month'),
      'login' => strtotime('-3 day'),
    ]);
  }

}
