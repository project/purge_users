<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Traits;

use Drupal\user\Entity\User;

/**
 * Provides a report about changes in users.
 */
trait ChangedUsersTrait {

  /**
   * Determines which users have changed since a previous state.
   *
   * @param \Drupal\user\UserInterface[] $original_users
   *   Format: $[$name] = $user.
   *   User objects containing data from an earlier time.
   *   The array keys can be the usernames, but they don't have to be.
   *
   * @return string[]
   *   Format: $[$name] = 'deleted'|'blocked'|'unblocked'|...
   *   Report about changes to the users.
   *   Only contains entries that differ from the original state.
   *   Array keys from the input are preserved.
   */
  protected function getUserStateChanges(array $original_users): array {
    // Load all current users from the database.
    $current_users = User::loadMultiple();

    $changes = [];
    foreach ($original_users as $name => $original_user) {
      $uid = $original_user->id();
      $current_user = $current_users[$uid] ?? NULL;
      if ($current_user === NULL) {
        $changes[$name] = 'deleted';
        continue;
      }
      /** @var \Drupal\user\UserInterface $current_user */
      $current_user = $this->userStorage->loadUnchanged($uid);
      if ($current_user === NULL) {
        $changes[$name] = 'deleted after cache clear';
        continue;
      }
      if ($original_user->isBlocked()) {
        if (!$current_user->isBlocked()) {
          // This should normally not happen.
          $changes[$name] = 'unblocked';
        }
      }
      else {
        if ($current_user->isBlocked()) {
          $changes[$name] = 'blocked';
        }
      }
    }

    return $changes;
  }

}
