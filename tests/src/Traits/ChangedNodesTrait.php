<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Traits;

use Drupal\node\Entity\Node;

/**
 * Provides a report about changes in nodes.
 */
trait ChangedNodesTrait {

  /**
   * Determines which nodes have changed since a previous state.
   *
   * @param \Drupal\node\NodeInterface[] $original_nodes
   *   Format: $[$key] = $node.
   *   Node objects containing data from an earlier time.
   *   The array keys can be anything.
   *
   * @return string[]
   *   Format: $[$key] = 'deleted'|'unpublished'|'anonymized'|...
   *   Report about changes to the nodes.
   *   Only contains entries that differ from the original state.
   *   Array keys from the input are preserved.
   */
  protected function getNodeStateChanges(array $original_nodes): array {
    $current_nodes = Node::loadMultiple();

    $changes = [];
    foreach ($original_nodes as $key => $original_node) {
      $nid = $original_node->id();
      if (!$nid) {
        // The node was not properly created in the first place, or something
        // unexpected happened to the node object since then.
        // Make sure this shows up in the diff, if this ever happens.
        $changes[$key] = 'nid: ' . var_export($nid, TRUE);
        continue;
      }
      $current_node = $current_nodes[$nid] ?? NULL;
      if ($current_node === NULL) {
        $changes[$key] = 'deleted';
        continue;
      }
      /** @var \Drupal\node\NodeInterface $current_node */
      $current_node = $this->nodeStorage->loadUnchanged($nid);
      if ($current_node === NULL) {
        // The node was deleted in the database, but it was still stuck in the
        // cache somehow.
        // Make sure this shows up in the diff, if this ever happens.
        $changes[$key] = 'deleted after cache clear';
        continue;
      }
      $parts = [];
      if (!$current_node->isPublished()) {
        $parts[] = 'unpublished';
      }
      $original_uid = $original_node->getOwnerId();
      $new_uid = $current_node->getOwnerId();
      if (!$new_uid) {
        $parts[] = 'anonymized';
      }
      elseif ($new_uid != $original_uid) {
        // This is unexpected.
        $parts[] = 'assigned to ' . $new_uid;
      }
      if (!$parts) {
        continue;
      }
      $changes[$key] = implode(', ', $parts);
    }

    return $changes;
  }

}
