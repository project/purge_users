<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Kernel;

use Drupal\Tests\purge_users\Traits\ChangedNodesTrait;
use Drupal\Tests\purge_users\Traits\ChangedUsersTrait;
use Drupal\node\Entity\Node;

/**
 * Covers what happens to purged users and their content.
 *
 * @group purge_users
 */
class PurgeMethodTest extends KernelTestBase {

  use ChangedUsersTrait;
  use ChangedNodesTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'user', 'node', 'purge_users'];

  /**
   * Node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('node');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $etm */
    $etm = $this->container->get('entity_type.manager');
    $this->nodeStorage = $etm->getStorage('node');
  }

  /**
   * Tests different purge methods.
   *
   * @param string[] $expected_user_states
   *   Format: $[$key] = 'deleted'|'blocked'|'unblocked'|...
   *   Only contains entries that differ from the original state.
   * @param string[] $expected_node_states
   *   Format: $[$key] = 'deleted'|'published'|'unpublished'|...
   *   Only contains entries that differ from the original state.
   * @param array $settings
   *   Format: $[$name][$key] = $value.
   * @param string $mode
   *   One of 'cron' or 'form'.
   *
   * @dataProvider providerPurgeMethod
   */
  public function testPurgeMethod(array $expected_user_states, array $expected_node_states, array $settings, string $mode): void {
    $this->config('purge_users.settings')
      // Set a purge condition.
      ->set('user_never_lastlogin_value', '2')
      ->set('user_never_lastlogin_period', 'year')
      ->set('enabled_never_loggedin_users', TRUE)
      ->save();

    // Settings from the data provider.
    // Usually this will be the purge method.
    foreach ($settings as $name => $values) {
      $config = $this->config($name);
      foreach ($values as $k => $v) {
        $config->set($k, $v);
      }
      $config->save();
    }

    // Don't call strtotime repeatedly within the loop.
    $one_year_ago = strtotime('-1 year');
    $three_years_ago = strtotime('-3 year');

    // phpcs:disable
    // Keep these aligned for easy comparison.
    $user_values = [
      'created3y' =>         ['created' => $three_years_ago, 'status' => 1],
      'created3y_blocked' => ['created' => $three_years_ago, 'status' => 0],
      'created1y' =>         ['created' => $one_year_ago,    'status' => 1],
      'created1y_blocked' => ['created' => $one_year_ago,    'status' => 0],
    ];
    // phpcs:enable

    // Create users and nodes.
    $original_users = [];
    $original_nodes = [];
    foreach ($user_values as $name => $values) {
      $values['login'] = 0;
      $user = $this->createUser(
        [],
        $name,
        FALSE,
        $values);
      $original_users[$name] = clone $user;
      // Create an example node for each user.
      $node = Node::create([
        'uid' => $user->id(),
        'published' => 1,
        'title' => "Node by $name",
        'type' => 'page',
      ]);
      $node->save();
      $original_nodes[$name] = clone $node;
    }

    $this->runPurgeOperation($mode);

    self::assertSame(
      [
        'users' => $expected_user_states,
        'nodes' => $expected_node_states,
      ],
      [
        'users' => $this->getUserStateChanges($original_users),
        'nodes' => $this->getNodeStateChanges($original_nodes),
      ]);
  }

  /**
   * Data provider.
   *
   * @return array[]
   *   Format: $[$dataset_name] = [
   *     $expected_user_states,
   *     $expected_node_states,
   *     $settings,
   *     'cron'|'form',
   *   ].
   */
  public static function providerPurgeMethod(): array {
    $basics = [
      'user_cancel_delete' => [
        [
          'created3y' => 'deleted',
          'created3y_blocked' => 'deleted',
        ],
        [
          'created3y' => 'deleted',
          'created3y_blocked' => 'deleted',
        ],
      ],
      'user_cancel_reassign' => [
        [
          'created3y' => 'deleted',
          'created3y_blocked' => 'deleted',
        ],
        [
          'created3y' => 'anonymized',
          'created3y_blocked' => 'anonymized',
        ],
      ],
      'user_cancel_block' => [
        [
          'created3y' => 'blocked',
        ],
        [],
      ],
      'user_cancel_block_unpublish' => [
        [
          'created3y' => 'blocked',
        ],
        [
          'created3y' => 'unpublished',
          'created3y_blocked' => 'unpublished',
        ],
      ],
    ];
    $datasets = [];
    // phpcs:ignore
    foreach ($basics as $method => [$expected_user_states, $expected_node_states]) {
      // Create one dataset with the purge method configured directly with
      // purge_users module.
      $datasets[$method] = [
        $expected_user_states,
        $expected_node_states,
        [
          'purge_users.settings' => [
            'purge_user_cancel_method' => $method,
          ],
        ],
      ];
      // Create another dataset using 'site_policy', with the purge method
      // configured through the core user module.
      // The expected states after purge are the same.
      $datasets["$method.site_policy"] = [
        $expected_user_states,
        $expected_node_states,
        [
          'purge_users.settings' => [
            'purge_user_cancel_method' => 'user_cancel_site_policy',
          ],
          'user.settings' => [
            'cancel_method' => $method,
          ],
        ],
      ];
    }
    // Run the same tests both with cron and with web UI purge.
    return static::duplicateAndAppendPurgeMode($datasets);
  }

}
