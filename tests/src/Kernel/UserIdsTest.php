<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Kernel;

use Drupal\Component\Utility\Variable;

/**
 * Tests the function that finds user ids to purge or to modify.
 *
 * @group purge_users
 *
 * @see purge_users_get_user_ids()
 */
class UserIdsTest extends KernelTestBase {

  /**
   * Asserts that two values are the same, with a more helpful diff.
   *
   * @param string[] $expected
   *   Expected names.
   * @param string[] $actual
   *   Actual names.
   */
  protected static function assertSameExported(array $expected, array $actual): void {
    self::assertSame(
      Variable::export($expected),
      Variable::export($actual));
  }

  /**
   * Tests user ids.
   *
   * @param string[] $expected_names
   *   User names for the ids returned from purge_users_get_user_ids().
   *   Format: $[] = $username.
   * @param string $op
   *   One of 'purge_users' or 'notification_users'.
   * @param array $settings
   *   Settings for any module.
   *   Format: $[$name][$k] = $v.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @dataProvider userIdsByUserValuesProvider
   */
  public function testUserIdsByUserValues(array $expected_names, string $op, array $settings): void {
    $this->setSettings($settings);

    // Don't call strtotime repeatedly.
    $one_year_ago = strtotime('-1 year');
    $two_years_ago = strtotime('-2 year');
    $three_years_ago = strtotime('-3 year');

    // phpcs:disable
    // Keep these values on single lines, to compare them more easily.
    // Note that some permutations are excluded, because the created date must
    // always be earlier or equal to the last login date.
    $user_values = [
      'created3y_loginNever_blocked' => ['created' => $three_years_ago, 'login' => 0,                'status' => 0],
      'created3y_loginNever' =>         ['created' => $three_years_ago, 'login' => 0,                'status' => 1],
      'created3y_login3y_blocked' =>    ['created' => $three_years_ago, 'login' => $three_years_ago, 'status' => 0],
      'created3y_login3y' =>            ['created' => $three_years_ago, 'login' => $three_years_ago, 'status' => 1],
      'created3y_login2y_blocked' =>    ['created' => $three_years_ago, 'login' => $two_years_ago,   'status' => 0],
      'created3y_login2y' =>            ['created' => $three_years_ago, 'login' => $two_years_ago,   'status' => 1],
      'created3y_login1y_blocked' =>    ['created' => $three_years_ago, 'login' => $one_year_ago,    'status' => 0],
      'created3y_login1y' =>            ['created' => $three_years_ago, 'login' => $one_year_ago,    'status' => 1],
      'created2y_loginNever_blocked' => ['created' => $two_years_ago,   'login' => 0,                'status' => 0],
      'created2y_loginNever' =>         ['created' => $two_years_ago,   'login' => 0,                'status' => 1],
      'created2y_login2y_blocked' =>    ['created' => $two_years_ago,   'login' => $two_years_ago,   'status' => 0],
      'created2y_login2y' =>            ['created' => $two_years_ago,   'login' => $two_years_ago,   'status' => 1],
      'created2y_login1y_blocked' =>    ['created' => $two_years_ago,   'login' => $one_year_ago,    'status' => 0],
      'created2y_login1y' =>            ['created' => $two_years_ago,   'login' => $one_year_ago,    'status' => 1],
      'created1y_loginNever_blocked' => ['created' => $one_year_ago,    'login' => 0,                'status' => 0],
      'created1y_loginNever' =>         ['created' => $one_year_ago,    'login' => 0,                'status' => 1],
      'created1y_login1y_blocked' =>    ['created' => $one_year_ago,    'login' => $one_year_ago,    'status' => 0],
      'created1y_login1y' =>            ['created' => $one_year_ago,    'login' => $one_year_ago,    'status' => 1],

    ];
    // phpcs:enable

    // Create users.
    $names_by_uid = [];
    foreach ($user_values as $name => $values) {
      $user = $this->createUser(
        [],
        $name,
        FALSE,
        $values);
      $names_by_uid[$user->id()] = $name;
    }

    $actual_uids = purge_users_get_user_ids($op);
    // User ids have the order in which they were created.
    sort($actual_uids);

    // Make sure that duplicate uids are detected.
    $actual_names = [];
    foreach ($actual_uids as $uid) {
      $actual_names[] = $names_by_uid[$uid];
    }

    self::assertSameExported($expected_names, $actual_names);
  }

  /**
   * Data provider.
   *
   * @return \Iterator|array[]
   *   Format: $[$dataset_name] = [
   *     $names_to_purge : string[],
   *     $names_to_notify : string[],
   *     $purge_users_settings : array,
   *     $other_settings : array,  // optional.
   *   ].
   */
  public static function userIdsByUserValuesProvider(): \Iterator {
    yield from static::buildDatasets(
      'none',
      [],
      [],
      []);
    yield from static::buildDatasets(
      'notify_all_purge_none',
      [],
      [
        'created3y_loginNever_blocked',
        'created3y_loginNever',
        'created3y_login3y_blocked',
        'created3y_login3y',
        'created3y_login2y_blocked',
        'created3y_login2y',
        'created3y_login1y_blocked',
        'created3y_login1y',
        'created2y_loginNever_blocked',
        'created2y_loginNever',
        'created2y_login2y_blocked',
        'created2y_login2y',
        'created2y_login1y_blocked',
        'created2y_login1y',
        'created1y_loginNever_blocked',
        'created1y_loginNever',
        'created1y_login1y_blocked',
        'created1y_login1y',
      ],
      [
        // Enable a combination of purge conditions, with long timespan.
        'enabled_never_loggedin_users' => TRUE,
        'user_never_lastlogin_value' => 5 * 12,
        'user_never_lastlogin_period' => 'month',
        'enabled_loggedin_users' => TRUE,
        'user_lastlogin_value' => 5 * 12,
        'user_lastlogin_period' => 'month',
        // Enable pre-notifications, with short timespan.
        'user_before_notification_value' => '2',
        'user_before_notification_period' => 'month',
        'send_email_user_before_notification' => TRUE,
      ]);
    yield from static::buildDatasets(
      'all_purge_conditions',
      [
        'created3y_loginNever_blocked',
        'created3y_loginNever',
        'created3y_login3y_blocked',
        'created3y_login3y',
      ],
      [
        'created3y_login2y_blocked',
        'created3y_login2y',
        'created2y_loginNever_blocked',
        'created2y_loginNever',
        'created2y_login2y_blocked',
        'created2y_login2y',
      ],
      [
        // Enable all purge conditions, with 2.5 years timespan.
        'enabled_never_loggedin_users' => TRUE,
        'user_never_lastlogin_value' => 30,
        'user_never_lastlogin_period' => 'month',
        'enabled_loggedin_users' => TRUE,
        'user_lastlogin_value' => 30,
        'user_lastlogin_period' => 'month',
        'enabled_inactive_users' => TRUE,
        'user_inactive_value' => 30,
        'user_inactive_period' => 'month',
        'enabled_blocked_users' => TRUE,
        'user_blocked_value' => 30,
        'user_blocked_period' => 'month',
        // Enable pre-notifications, with 1.5 years timespan.
        'user_before_notification_value' => 18,
        'user_before_notification_period' => 'month',
        'send_email_user_before_notification' => TRUE,
      ]);
    yield from static::buildDatasets(
      'mixed',
      [
        'created3y_login3y_blocked',
        'created3y_login3y',
      ],
      [
        'created3y_login2y_blocked',
        'created3y_login2y',
        'created2y_login2y_blocked',
        'created2y_login2y',
      ],
      [
        // Enable a purge condition after 2.5 years.
        'enabled_loggedin_users' => TRUE,
        'user_lastlogin_value' => '30',
        'user_lastlogin_period' => 'month',
        // Enable pre-notifications after 1.5 years.
        'user_before_notification_value' => '18',
        'user_before_notification_period' => 'month',
        'send_email_user_before_notification' => TRUE,
      ]);
    yield from static::buildDatasets(
      'different_time_unit',
      [
        'created3y_login3y_blocked',
        'created3y_login3y',
      ],
      [
        'created3y_login2y_blocked',
        'created3y_login2y',
        'created2y_login2y_blocked',
        'created2y_login2y',
      ],
      [
        // Enable a purge condition after 2.5 years.
        'enabled_loggedin_users' => TRUE,
        'user_lastlogin_value' => '30',
        'user_lastlogin_period' => 'month',
        // Enable pre-notifications after 1.5 years.
        // Use time unit day, instead of month.
        // This was broken in a prior version.
        'user_before_notification_value' => 18 * 30,
        'user_before_notification_period' => 'day',
        'send_email_user_before_notification' => TRUE,
      ]);
  }

  /**
   * Builds two datasets from seed data: One for purge, one for notify.
   *
   * @param string $name
   *   First part of dataset name.
   * @param array $names_to_purge
   *   Expected user names to be purged.
   * @param array $names_to_notify
   *   Expected user names to be pre-notified.
   * @param array $purge_users_settings
   *   Configuration in 'purge_users.settings'.
   * @param array $other_settings
   *   Additional configuration for other modules.
   *
   * @return \Iterator|array[]
   *   Format: $[$dataset_name] = [
   *     $names_to_purge : string[],
   *     $names_to_notify : string[],
   *     $purge_users_settings : array,
   *     $other_settings : array,  // optional.
   *   ].
   */
  protected static function buildDatasets(
    string $name,
    array $names_to_purge,
    array $names_to_notify,
    array $purge_users_settings,
    array $other_settings = [],
  ): \Iterator {
    $settings = $other_settings;
    foreach ($purge_users_settings as $k => $v) {
      $settings['purge_users.settings'][$k] = $v;
    }
    yield "$name.purge" => [
      $names_to_purge,
      'purge_users',
      $settings,
    ];
    yield "$name.notify" => [
      $names_to_notify,
      'notification_users',
      $settings,
    ];
  }

}
