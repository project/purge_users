<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Kernel;

use Drupal\Tests\purge_users\Traits\ChangedUsersTrait;

/**
 * Covers settings that control which users get purged, and when.
 *
 * @group purge_users
 */
class PurgeConditionTest extends KernelTestBase {

  use ChangedUsersTrait;

  /**
   * Tests the effect of the 'purge_on_cron' setting.
   *
   * @param string $mode
   *   One of 'form' or 'cron'.
   * @param bool $purge_on_cron
   *   TRUE, to enable the 'purge_on_cron' setting.
   * @param string[] $expected
   *   Expected user state changes with the given dataset.
   *   Format: $[$name] = 'deleted'.
   *
   * @testWith ["cron", true, {"created3y": "deleted"}]
   *           ["cron", false, []]
   *           ["form", true, {"created3y": "deleted"}]
   *           ["form", false, {"created3y": "deleted"}]
   */
  public function testPurgeOnCron(string $mode, bool $purge_on_cron, array $expected): void {
    $this->config('purge_users.settings')
      // Set a purge condition.
      ->set('user_never_lastlogin_value', '1')
      ->set('user_never_lastlogin_period', 'year')
      ->set('enabled_never_loggedin_users', TRUE)
      // Set purge method.
      ->set('purge_user_cancel_method', 'user_cancel_delete')
      // Set purge on cron.
      ->set('purge_on_cron', $purge_on_cron)
      ->save();

    // Don't call strtotime repeatedly within the loop.
    $one_year_ago = strtotime('-1 year');
    $three_years_ago = strtotime('-3 year');

    $user_values = [
      'created3y' => ['created' => $three_years_ago],
      'created1y' => ['created' => $one_year_ago],
    ];

    // Create users.
    $original_users = [];
    foreach ($user_values as $name => $values) {
      $values['login'] = 0;
      $original_users[$name] = clone $this->createUser(
        [],
        $name,
        FALSE,
        $values);
    }

    $this->runPurgeOperation($mode);

    $actual = $this->getUserStateChanges($original_users);

    self::assertSame($expected, $actual);
  }

  /**
   * Tests conditions that are based on user roles.
   *
   * @param string $mode
   *   One of 'form' or 'cron'.
   *
   * @testWith ["cron"]
   *           ["form"]
   */
  public function testRoleConditions(string $mode): void {
    $this->config('purge_users.settings')
      // @todo Test roles with all condition types.
      ->set('user_never_lastlogin_value', '2')
      ->set('user_never_lastlogin_period', 'year')
      ->set('enabled_never_loggedin_users', TRUE)
      // Include some roles, exclude others.
      ->set('purge_excluded_users_roles', ['administrator', 'moderator'])
      ->set('purge_included_users_roles', ['authenticated', 'contributor'])
      // Set purge method.
      ->set('purge_user_cancel_method', 'user_cancel_delete')
      ->save();

    $this->createRole([], 'moderator', 'Moderator');
    $this->createRole([], 'contributor', 'Contributor');

    // Don't call strtotime repeatedly within the loop.
    $one_year_ago = strtotime('-1 year');
    $three_years_ago = strtotime('-3 year');

    // phpcs:disable
    // Keep these values on single lines, to compare them more easily.
    $user_values = [
      'uBasic_created1y' =>                ['created' => $one_year_ago,    'roles' => []],
      'uModerator_created1y' =>            ['created' => $one_year_ago,    'roles' => ['moderator']],
      'uContributor_created1y' =>          ['created' => $one_year_ago,    'roles' => ['contributor']],
      'uContributorModerator_created1y' => ['created' => $one_year_ago,    'roles' => ['moderator', 'contributor']],
      'uBasic_created3y' =>                ['created' => $three_years_ago, 'roles' => []],
      'uModerator_created3y' =>            ['created' => $three_years_ago, 'roles' => ['moderator']],
      'uContributor_created3y' =>          ['created' => $three_years_ago, 'roles' => ['contributor']],
      'uContributorModerator_created3y' => ['created' => $three_years_ago, 'roles' => ['moderator', 'contributor']],
    ];
    // phpcs:enable

    // Create users.
    $original_users = [];
    foreach ($user_values as $name => $values) {
      $roles = $values['roles'];
      unset($values['roles']);
      $values['login'] = 0;
      $user = $this->createUser([], $name, FALSE, $values);
      foreach ($roles as $role) {
        $user->addRole($role);
      }
      $user->save();
      $original_users[$name] = clone $user;
    }

    $this->runPurgeOperation($mode);

    $actual = $this->getUserStateChanges($original_users);

    $expected = [
      'uBasic_created3y' => 'deleted',
      'uContributor_created3y' => 'deleted',
    ];

    self::assertSame($expected, $actual);
  }

  /**
   * Tests conditions that are based on created, status and login fields.
   *
   * @param string[] $expected
   *   Format: $[$key] = 'deleted'|'blocked'|'unblocked'|...
   *   Only contains entries that differ from the original state.
   * @param array $settings
   *   Settings within 'purge_users.settings'.
   * @param string $mode
   *   One of 'cron' or 'form'.
   *
   * @dataProvider userConditionsProvider
   */
  public function testUserConditions(array $expected, array $settings, string $mode): void {
    $config = $this->config('purge_users.settings');
    $config->set('purge_user_cancel_method', 'user_cancel_delete');
    foreach ($settings as $k => $v) {
      $config->set($k, $v);
    }
    $config->save();

    // Don't call strtotime repeatedly.
    $one_year_ago = strtotime('-1 year');
    $three_years_ago = strtotime('-3 year');

    // phpcs:disable
    // Keep these values on single lines, to compare them more easily.
    // Note that some permutations are excluded, because the created date must
    // always be earlier or equal to the last login date.
    $user_values = [
      'created3y_loginNever_blocked' => ['created' => $three_years_ago, 'login' => 0,                'status' => 0],
      'created3y_loginNever' =>         ['created' => $three_years_ago, 'login' => 0,                'status' => 1],
      'created3y_login3y_blocked' =>    ['created' => $three_years_ago, 'login' => $three_years_ago, 'status' => 0],
      'created3y_login3y' =>            ['created' => $three_years_ago, 'login' => $three_years_ago, 'status' => 1],
      'created3y_login1y_blocked' =>    ['created' => $three_years_ago, 'login' => $one_year_ago,    'status' => 0],
      'created3y_login1y' =>            ['created' => $three_years_ago, 'login' => $one_year_ago,    'status' => 1],
      'created1y_loginNever_blocked' => ['created' => $one_year_ago,    'login' => 0,                'status' => 0],
      'created1y_loginNever' =>         ['created' => $one_year_ago,    'login' => 0,                'status' => 1],
      'created1y_login1y_blocked' =>    ['created' => $one_year_ago,    'login' => $one_year_ago,    'status' => 0],
      'created1y_login1y' =>            ['created' => $one_year_ago,    'login' => $one_year_ago,    'status' => 1],
    ];
    // phpcs:enable

    // Create users.
    $original_user_objects = [];
    foreach ($user_values as $name => $values) {
      $original_user_objects[$name] = clone $this->createUser(
        [],
        $name,
        FALSE,
        $values);
    }

    $this->runPurgeOperation($mode);

    $actual = $this->getUserStateChanges($original_user_objects);

    self::assertSame($expected, $actual);
  }

  /**
   * Data provider.
   *
   * @return array[]
   *   Format: $[$dataset_name] = [
   *     $expected_user_state_changes,
   *     $purge_users_config,
   *     'cron'|'form',
   *   ].
   */
  public static function userConditionsProvider(): array {
    $value = 2;
    $period = 'year';
    return static::duplicateAndAppendPurgeMode([
      'purge_none' => [
        [
          // No users are purged.
        ],
        [
          // No purge conditions are enabled.
        ],
      ],
      'purge_all' => [
        [
          // All users are purged.
          // This dataset proves that all users were properly created.
          'created3y_loginNever_blocked' => 'deleted',
          'created3y_loginNever' => 'deleted',
          'created3y_login3y_blocked' => 'deleted',
          'created3y_login3y' => 'deleted',
          'created3y_login1y_blocked' => 'deleted',
          'created3y_login1y' => 'deleted',
          'created1y_loginNever_blocked' => 'deleted',
          'created1y_loginNever' => 'deleted',
          'created1y_login1y_blocked' => 'deleted',
          'created1y_login1y' => 'deleted',
        ],
        [
          // Enable a combination of purge conditions, with short timespan.
          'enabled_never_loggedin_users' => TRUE,
          'user_never_lastlogin_value' => 2,
          'user_never_lastlogin_period' => 'day',
          'enabled_loggedin_users' => TRUE,
          'user_lastlogin_value' => 2,
          'user_lastlogin_period' => 'day',
        ],
      ],
      'purge_all_methods' => [
        [
          // All users are purged, like above.
          'created3y_loginNever_blocked' => 'deleted',
          'created3y_loginNever' => 'deleted',
          'created3y_login3y_blocked' => 'deleted',
          'created3y_login3y' => 'deleted',
          'created3y_login1y_blocked' => 'deleted',
          'created3y_login1y' => 'deleted',
          'created1y_loginNever_blocked' => 'deleted',
          'created1y_loginNever' => 'deleted',
          'created1y_login1y_blocked' => 'deleted',
          'created1y_login1y' => 'deleted',
        ],
        [
          // All purge methods are enabled, with short timespan.
          // This used to be broken in previous versions, but now it works.
          'enabled_never_loggedin_users' => TRUE,
          'user_never_lastlogin_value' => 2,
          'user_never_lastlogin_period' => 'day',
          'enabled_loggedin_users' => TRUE,
          'user_lastlogin_value' => 2,
          'user_lastlogin_period' => 'day',
          'enabled_inactive_users' => TRUE,
          'user_inactive_value' => 2,
          'user_inactive_period' => 'day',
          'enabled_blocked_users' => TRUE,
          'user_blocked_value' => 2,
          'user_blocked_period' => 'day',
        ],
      ],
      'never_logged_in' => [
        [
          'created3y_loginNever_blocked' => 'deleted',
          'created3y_loginNever' => 'deleted',
        ],
        [
          'enabled_never_loggedin_users' => TRUE,
          'user_never_lastlogin_value' => $value,
          'user_never_lastlogin_period' => $period,
        ],
      ],
      'not_been_active_for' => [
        [
          'created3y_loginNever_blocked' => 'deleted',
        ],
        [
          'enabled_inactive_users' => TRUE,
          'user_inactive_value' => $value,
          'user_inactive_period' => $period,
        ],
      ],
      'blocked_for' => [
        [
          'created3y_login3y_blocked' => 'deleted',
        ],
        [
          'enabled_blocked_users' => TRUE,
          'user_blocked_value' => $value,
          'user_blocked_period' => $period,
        ],
      ],
      'not_logged_in_for' => [
        [
          'created3y_login3y_blocked' => 'deleted',
          'created3y_login3y' => 'deleted',
        ],
        [
          'enabled_loggedin_users' => TRUE,
          'user_lastlogin_value' => $value,
          'user_lastlogin_period' => $period,
        ],
      ],
      'not_logged_in_for.disregard_blocked' => [
        [
          'created3y_login3y' => 'deleted',
        ],
        [
          'enabled_loggedin_users' => TRUE,
          'user_lastlogin_value' => $value,
          'user_lastlogin_period' => $period,
          'disregard_blocked_users' => TRUE,
        ],
      ],
      'not_logged_in_for.block_on_purge' => [
        [
          'created3y_login3y' => 'blocked',
        ],
        [
          'enabled_loggedin_users' => TRUE,
          'user_lastlogin_value' => $value,
          'user_lastlogin_period' => $period,
          'purge_user_cancel_method' => 'user_cancel_block',
        ],
      ],
      'not_logged_in_for.disregard_blocked.block_on_purge' => [
        [
          'created3y_login3y' => 'blocked',
        ],
        [
          'enabled_loggedin_users' => TRUE,
          'user_lastlogin_value' => $value,
          'user_lastlogin_period' => $period,
          'disregard_blocked_users' => TRUE,
          'purge_user_cancel_method' => 'user_cancel_block',
        ],
      ],
    ]);
  }

}
