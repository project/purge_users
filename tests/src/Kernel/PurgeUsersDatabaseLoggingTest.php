<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests logging user id and notification types.
 *
 * The user id and notification type are saved in the
 * table purge_users_notifications.
 *
 * @group purge_users
 */
class PurgeUsersDatabaseLoggingTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'purge_users',
  ];

  /**
   * Tests logging of user id and notification type.
   */
  public function testUserIdNotificationTypeLogging() {
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('purge_users', ['purge_users_notifications']);
    $this->installEntitySchema('user');

    /** @var \Drupal\purge_users\Services\UserManagementServiceInterface $purge_users_manager */
    $purge_users_manager = $this->container->get('purge_users.user_management');

    $admin = $this->createUser([], NULL, TRUE);
    $user = $this->createUser();

    // Assert user ids are not in the table before notification.
    $this->assertFalse($purge_users_manager->userIsNotified($user->id(), 'notification_users'));
    $this->assertFalse($purge_users_manager->userIsNotified($admin->id(), 'notification_users'));
    $this->assertFalse($purge_users_manager->userIsNotified($user->id(), 'purge_users'));
    $this->assertFalse($purge_users_manager->userIsNotified($admin->id(), 'purge_users'));

    // Flag user as notified.
    $purge_users_manager->flagUserAsNotified($user->id(), 'notification_users');
    // Verify user is notified.
    $this->assertTrue($purge_users_manager->userIsNotified($user->id(), 'notification_users'));
    // The user only has one notification type.
    $this->assertFalse($purge_users_manager->userIsNotified($user->id(), 'purge_users'));

    // Delete user, verify the user id is removed.
    $user->delete();
    $this->assertFalse($purge_users_manager->userIsNotified($user->id(), 'notification_users'));
    $this->assertFalse($purge_users_manager->userIsNotified($user->id(), 'purge_users'));
  }

}
