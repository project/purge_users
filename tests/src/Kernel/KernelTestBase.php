<?php

declare(strict_types=1);

namespace Drupal\Tests\purge_users\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase as CoreKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\purge_users\Form\ConfirmationForm;

/**
 * Base class for kernel tests.
 *
 * @group purge_users
 */
abstract class KernelTestBase extends CoreKernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'user', 'purge_users'];

  /**
   * User storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The cron service.
   *
   * @var \Drupal\Core\Cron
   */
  protected $cron;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->initKernelTest();
    $this->setBasicConfig();

    // Occupy user id 1 with an admin account.
    // Otherwise we get confusing test results.
    $admin = $this->createUser([], 'admin', TRUE, [
      'created' => strtotime('-1 year'),
      'login' => 0,
      'status' => 0,
    ]);
    self::assertEquals(1, $admin->id());
  }

  /**
   * Initializes services needed for a kernel test.
   */
  protected function initKernelTest() {
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('purge_users', ['purge_users_notifications']);
    $this->installEntitySchema('user');

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $etm */
    $etm = $this->container->get('entity_type.manager');
    $this->userStorage = $etm->getStorage('user');
    $this->cron = $this->container->get('cron');

    $this->config('system.site')
      // Replicate settings from browser tests, to have same expected emails.
      ->set('mail', 'simpletest@example.com')
      ->set('name', 'Drupal')
      ->save();
  }

  /**
   * Sets a baseline configuration for all tests.
   */
  protected function setBasicConfig(): void {
    $this->config('purge_users.settings')
      // Disable all the purge conditions.
      ->set('enabled_inactive_users', FALSE)
      ->set('enabled_loggedin_users', FALSE)
      ->set('enabled_never_loggedin_users', FALSE)
      ->set('enabled_blocked_users', FALSE)
      // Exclude admin, include regular users.
      ->set('purge_excluded_users_roles', ['administrator'])
      ->set('purge_included_users_roles', ['authenticated'])
      // Disable notifications on purge, but set default text.
      ->set('send_email_notification', FALSE)
      ->set('inactive_user_notify_subject', 'Dear user')
      ->set('inactive_user_notify_text', 'Dear User, Your account has been deleted due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website. Thank you')
      // Disable notifications before purge, but set default text.
      ->set('user_before_deletion_subject', 'Dear user')
      ->set('send_email_user_before_notification', FALSE)
      ->set('user_before_deletion_text', 'Dear User, Your account will be deleted soon due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website. Thank you')
      // Purge when running cron.
      ->set('purge_on_cron', TRUE)
      ->save();
  }

  /**
   * Imports settings.
   *
   * @param array $settings
   *   Format: $[$name][$key] = $value.
   */
  protected function setSettings(array $settings): void {
    foreach ($settings as $name => $values) {
      $config = $this->config($name);
      foreach ($values as $k => $v) {
        $config->set($k, $v);
      }
      $config->save();
    }
  }

  /**
   * Runs the purge operation, either as cron or form submit.
   *
   * @param string $mode
   *   One of 'cron' or 'form'.
   */
  protected function runPurgeOperation(string $mode): void {
    if ($mode === 'form') {
      $form_object = new ConfirmationForm();
      $form_state = new FormState();
      $form = [];
      $form_object->submitForm($form, $form_state);
      $batch = &batch_get();
      if ($batch) {
        $batch['progressive'] = FALSE;
        batch_process();
      }
    }
    else {
      $this->cron->run();
    }
  }

  /**
   * Duplicates existing datasets, appending a new parameter 'form'|'cron'.
   *
   * @param array[] $datasets
   *   Original datasets.
   *   Format: $[$dataset_name] = [...$args].
   *
   * @return array[]
   *   New datasets.
   *   Format: $[$dataset_name . '.form'|'.cron'] = [...$args, 'cron'|'form'].
   */
  protected static function duplicateAndAppendPurgeMode(array $datasets): array {
    $new_datasets = [];
    foreach ($datasets as $dataset_name => $dataset) {
      $new_datasets["$dataset_name.cron"] = array_merge($dataset, ['cron']);
      $new_datasets["$dataset_name.form"] = array_merge($dataset, ['form']);
    }
    return $new_datasets;
  }

}
