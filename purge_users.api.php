<?php

/**
 * @file
 * Describes hooks and plugins provided by the Purge Users module.
 */

/**
 * Alter the never loggedin user IDs result.
 *
 * @param int[] $uids
 *   An array of user IDs.
 */
function hook_purge_never_loggedin_user_ids_alter(array &$uids) {
  // Do something.
}

/**
 * Alter the not loggedin user IDs result.
 *
 * @param int[] $uids
 *   An array of user IDs.
 */
function hook_purge_not_loggedin_user_ids_alter(array &$uids) {
  // Do something.
}

/**
 * Alter the inactive user IDs result.
 *
 * @param int[] $uids
 *   An array of user IDs.
 */
function hook_purge_inactive_user_ids_alter(array &$uids) {
  // Do something.
}

/**
 * Alter the blocked user IDs result.
 *
 * @param int[] $uids
 *   An array of user IDs.
 */
function hook_purge_blocked_user_ids_alter(array &$uids) {
  // Do something.
}
