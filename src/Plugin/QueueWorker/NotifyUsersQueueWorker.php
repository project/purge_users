<?php

declare(strict_types=1);

namespace Drupal\purge_users\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes cron queue.
 *
 * @QueueWorker(
 *   id = "notification_users",
 *   title = @Translation("Notify Users Tasks Worker: Notification Users"),
 *   cron = {"time" = 15}
 * )
 */
class NotifyUsersQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The purge user manager.
   *
   * @var \Drupal\purge_users\Services\UserManagementServiceInterface
   */
  protected $purgeUserManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->purgeUserManager = $container->get('purge_users.user_management');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($user_id): void {
    /** @var \Drupal\user\UserInterface|null $account */
    $account = $this->entityTypeManager->getStorage('user')->load($user_id);
    if (!$account) {
      return;
    }
    $this->purgeUserManager->notifyUser($account);
  }

}
