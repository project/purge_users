<?php

declare(strict_types=1);

namespace Drupal\purge_users\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes cron queue.
 *
 * @QueueWorker(
 *   id = "purge_users",
 *   title = @Translation("Purge Users Tasks Worker: Purge Users"),
 *   cron = {"time" = 15}
 * )
 */
class PurgeUsersQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The purge user manager.
   *
   * @var \Drupal\purge_users\Services\UserManagementServiceInterface
   */
  protected $purgeUserManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->purgeUserManager = $container->get('purge_users.user_management');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($user_id) {
    $account = $this->entityTypeManager->getStorage('user')->load($user_id);
    if (!$account) {
      return;
    }
    $config = $this->configFactory->get('purge_users.settings');
    $method = $config->get('purge_user_cancel_method') !== 'user_cancel_site_policy' ? $config->get('purge_user_cancel_method') : $this->configFactory->get('user.settings')
      ->get('cancel_method');
    $this->purgeUserManager->purgeUser($account, $method);
  }

}
