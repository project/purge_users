<?php

declare(strict_types=1);

namespace Drupal\purge_users\Plugin\BatchWorker;

use Drupal\user\Entity\User;

/**
 * Purge user and notify.
 *
 * @package Drupal\purge_users\Plugin\BatchWorker
 */
class BatchWorker {

  /**
   * Process items in a batch.
   */
  public static function batchWorkerPurgeUsers($id, &$context): void {
    $account = User::load($id);
    if (!isset($context['results']['processed'])) {
      $context['results']['processed'] = 0;
    }
    $config = \Drupal::config('purge_users.settings');
    $method = $config->get('purge_user_cancel_method') != 'user_cancel_site_policy' ? $config->get('purge_user_cancel_method') : \Drupal::config('user.settings')
      ->get('cancel_method');
    $name = $account->get('name')->value;
    /** @var \Drupal\purge_users\Services\UserManagementServiceInterface $userManagement */
    $userManagement = \Drupal::service('purge_users.user_management');
    $userManagement->purgeUser($account, $method);
    $context['message'] = "Now processing $name ...";

    // Update our progress information.
    $context['results']['processed']++;
  }

  /**
   * Sends users pre-notifications in a batch.
   */
  public static function batchWorkerNotifyUsers($id, &$context): void {
    $account = User::load($id);

    if (!isset($context['results']['processed'])) {
      $context['results']['processed'] = 0;
    }

    // Update our progress information.
    $context['results']['processed']++;
    if (!$account) {
      return;
    }
    $name = $account->get('name')->value;
    /** @var \Drupal\purge_users\Services\UserManagementServiceInterface $userManagement */
    $userManagement = \Drupal::service('purge_users.user_management');
    $userManagement->notifyUser($account);
    $context['message'] = "$name notified.";
  }

}
