<?php

namespace Drupal\purge_users\Services;

use Drupal\user\UserInterface;

/**
 * Contract for the purge service.
 */
interface UserManagementServiceInterface {

  /**
   * Cancel a user and anonymize it.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to block.
   * @param string $method
   *   The cancel method to use.
   */
  public function purgeUser(UserInterface $user, string $method);

  /**
   * Sends a user notification before cancel and anonymize it.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to block.
   */
  public function notifyUser(UserInterface $user): void;

  /**
   * Checks if a user is already notified.
   *
   * @param string $user_id
   *   The user id.
   * @param string $type
   *   The notification type.
   *
   * @return bool
   *   Return true if user id is found.
   */
  public function userIsNotified(string $user_id, string $type): bool;

  /**
   * Flags the user as notified.
   *
   * @param string $user_id
   *   The user id.
   * @param string $type
   *   The notification type.
   */
  public function flagUserAsNotified(string $user_id, string $type): void;

  /**
   * Removes the notification entry.
   *
   * @param string $user_id
   *   The user id.
   */
  public function removeNotificationFlags(string $user_id): void;

}
