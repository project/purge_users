<?php

namespace Drupal\purge_users\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure purge methods and conditions.
 *
 * @package Drupal\purge_users\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'purge_users_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('purge_users.settings');
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);
    $roles_array = array_map(function ($item) {
      return $item->label();
    }, $roles);
    $exclude_role_array = $roles_array;
    unset($exclude_role_array[RoleInterface::AUTHENTICATED_ID]);

    $notification_subject = $this->t('Your account is deleted');
    $notification_users_before_subject = $this->t('Your account will be deleted');
    $notification_text = $this->t("Dear User, \n\nYour account has been deleted due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website.\n\nThank you");
    $notification_users_before_text = $this->t("Dear User, \n\nYour account will be deleted soon due the website’s policy to automatically remove users who match certain criteria. If you have concerns regarding the deletion, please talk to the administrator of the website.\n\nThank you");
    $form['never_loggedin_user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Purge users who have never logged in for'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['never_loggedin_user']['user_never_lastlogin_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Value'),
      '#default_value' => $config->get('user_never_lastlogin_value'),
      '#prefix' => '<div class="purge-interval-selector clearfix">',
      '#attributes' => ['class' => ['purge-value']],
      '#states' => [
        'required' => [
          ':input[name="enabled_never_loggedin_users"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['never_loggedin_user']['user_never_lastlogin_period'] = [
      '#title' => $this->t('Period'),
      '#type' => 'select',
      '#options' => [
        'days' => $this->t('Days'),
        'month' => $this->t('Months'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => $config->get('user_never_lastlogin_period'),
      '#attributes' => ['class' => ['purge-period']],
      '#suffix' => '</div>',
    ];
    $form['never_loggedin_user']['enabled_never_loggedin_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled_never_loggedin_users'),
    ];

    $form['not_loggedin_user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Purge users who have not logged in for'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['not_loggedin_user']['user_lastlogin_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Value'),
      '#default_value' => $config->get('user_lastlogin_value'),
      '#prefix' => '<div class="purge-interval-selector clearfix">',
      '#attributes' => ['class' => ['purge-value']],
      '#states' => [
        'required' => [
          ':input[name="enabled_loggedin_users"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['not_loggedin_user']['user_lastlogin_period'] = [
      '#title' => $this->t('Period'),
      '#type' => 'select',
      '#options' => [
        'days' => $this->t('Days'),
        'month' => $this->t('Months'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => $config->get('user_lastlogin_period'),
      '#attributes' => ['class' => ['purge-period']],
      '#suffix' => '</div>',
    ];

    $form['not_loggedin_user']['enabled_loggedin_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled_loggedin_users'),
    ];

    $form['not_active_user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Purge users whose account has not been activated for'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['not_active_user']['user_inactive_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Value'),
      '#default_value' => $config->get('user_inactive_value'),
      '#prefix' => '<div class="purge-interval-selector clearfix">',
      '#attributes' => ['class' => ['purge-value']],
      '#states' => [
        'required' => [
          ':input[name="enabled_inactive_users"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['not_active_user']['user_inactive_period'] = [
      '#title' => $this->t('Period'),
      '#type' => 'select',
      '#options' => [
        'days' => $this->t('Days'),
        'month' => $this->t('Months'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => $config->get('user_inactive_period'),
      '#attributes' => ['class' => ['purge-period']],
      '#suffix' => '</div>',
    ];

    $form['not_active_user']['enabled_inactive_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled_inactive_users'),
    ];

    $form['blocked_user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Purge users who have been blocked for'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['blocked_user']['user_blocked_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Value'),
      '#default_value' => $config->get('user_blocked_value'),
      '#prefix' => '<div class="purge-interval-selector clearfix">',
      '#attributes' => ['class' => ['purge-value']],
      '#states' => [
        'required' => [
          ':input[name="enabled_blocked_users"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['blocked_user']['user_blocked_period'] = [
      '#title' => $this->t('Period'),
      '#type' => 'select',
      '#options' => [
        'days' => $this->t('Days'),
        'month' => $this->t('Months'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => $config->get('user_blocked_period'),
      '#attributes' => ['class' => ['purge-period']],
      '#suffix' => '</div>',
    ];

    $form['blocked_user']['enabled_blocked_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled_blocked_users'),
    ];

    $form['limit_roles'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Role limitation'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['limit_roles']['purge_included_users_roles'] = [
      '#title' => $this->t('Select roles to include in the purge'),
      '#description' => $this->t('Any user with one of the selected roles will be included, unless that user has a role among the one selected in the exclude roles field.'),
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#options' => $roles_array,
      '#default_value' => is_array($config->get('purge_included_users_roles')) ? $config->get('purge_included_users_roles') : [],
    ];

    $form['limit_roles']['purge_excluded_users_roles'] = [
      '#title' => $this->t('Select roles to exclude from the purge'),
      '#description' => $this->t('Any user with one of the selected role will be excluded.'),
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#options' => $exclude_role_array,
      '#default_value' => is_array($config->get('purge_excluded_users_roles')) ? $config->get('purge_excluded_users_roles') : [],
    ];

    $user_cancel_methods = user_cancel_methods();
    // Add the possibility to choose the site policy.
    $user_cancel_methods['#options']['user_cancel_site_policy'] = $this->t("Follow site's policy");

    $form['purge_user_cancel_method'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('When cancelling a user account'),
      '#default_value' => $config->get('purge_user_cancel_method'),
      '#options' => $user_cancel_methods['#options'],
    ];

    $form['disregard_blocked_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disregard inactive/blocked users'),
      '#default_value' => $config->get('disregard_blocked_users'),
      '#description' => $this->t('Do not look at inactive and blocked users. If you use a cancellation method that blocks users, this should normally be enabled, or blocked users will be included in the processing and make it unnecessarily heavy.'),
    ];

    $form['purge_on_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Purge on cron'),
      '#default_value' => $config->get('purge_on_cron'),
    ];

    $form['user_notification'] = [
      '#type' => 'details',
      '#title' => $this->t('User Deletion Notification'),
      '#open' => FALSE,
    ];

    $form['user_notification']['inactive_user_notify_subject'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Subject of user notification email'),
      '#default_value' => $config->get('inactive_user_notify_subject') ? $config->get('inactive_user_notify_subject') : $notification_subject,
      '#cols' => 1,
      '#rows' => 1,
      '#description' => $this->t('Customize the subject of the notification email sent to the user.'),
      '#required' => TRUE,
    ];

    $form['user_notification']['inactive_user_notify_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body of user notification email'),
      '#default_value' => $config->get('inactive_user_notify_text') ? $config->get('inactive_user_notify_text') : $notification_text,
      '#cols' => 70,
      '#rows' => 10,
      '#description' => $this->t('Customize the body of the notification email sent to the user.'),
      '#required' => TRUE,
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['user_notification']['token_help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['user'],
        '#show_restricted' => TRUE,
        '#show_nested' => FALSE,
      ];
    }

    $form['user_notification']['send_email_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Check to send email notification to purged users.'),
      '#default_value' => $config->get('send_email_notification'),
    ];

    $form['user_before_deletion'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification User Before Deletion'),
      '#open' => FALSE,
    ];
    $form['user_before_deletion']['user_before_deletion_subject'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Subject of user before deletion notification email'),
      '#default_value' => $config->get('user_before_deletion_subject') ? $config->get('user_before_deletion_subject') : $notification_users_before_subject,
      '#cols' => 1,
      '#rows' => 1,
      '#description' => $this->t('Customize the subject of the notification email sent to the user.'),
      '#required' => TRUE,
    ];
    $form['user_before_deletion']['user_before_deletion_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body of user before deletion notification email'),
      '#default_value' => $config->get('user_before_deletion_text') ? $config->get('user_before_deletion_text') : $notification_users_before_text,
      '#cols' => 70,
      '#rows' => 10,
      '#description' => $this->t('Customize the body of the notification email sent to the user.'),
      '#required' => TRUE,
    ];

    $form['user_before_deletion']['user_before_notification_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $config->get('user_before_notification_value'),
      '#prefix' => '<div class="purge-interval-selector clearfix">',
      '#attributes' => ['class' => ['purge-value']],
    ];
    $form['user_before_deletion']['user_before_notification_period'] = [
      '#title' => $this->t('Period'),
      '#type' => 'select',
      '#options' => [
        'days' => $this->t('Days'),
        'month' => $this->t('Months'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => $config->get('user_before_notification_period'),
      '#attributes' => ['class' => ['purge-period']],
      '#suffix' => '</div>',
    ];

    $form['user_before_deletion']['send_email_user_before_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Check to send email notification before purging users.'),
      '#default_value' => $config->get('send_email_user_before_notification'),
    ];

    $form['actions']['purge_users_now'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge users now'),
      '#attributes' => [
        'class' => [
          'purge-now',
          'button button--primary',
        ],
      ],
      '#submit' => ['::purgeUsersNowSubmit'],
    ];
    // Attach library.
    $form['#attached']['library'][] = 'purge_users/styling';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['purge_users.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $login_never_value = $form_state->getValue(['user_never_lastlogin_value']);
    $login_never_period = $form_state->getValue(['user_never_lastlogin_period']);
    $login_value = $form_state->getValue(['user_lastlogin_value']);
    $login_period = $form_state->getValue(['user_lastlogin_period']);
    $inactive_value = $form_state->getValue(['user_inactive_value']);
    $inactive_period = $form_state->getValue(['user_inactive_period']);
    $block_value = $form_state->getValue(['user_blocked_value']);
    $block_period = $form_state->getValue(['user_blocked_period']);
    $user_before_notification_period = $form_state->getValue(['user_before_notification_period']);
    $user_before_notification_value = $form_state->getValue(['user_before_notification_value']);
    $enable_blocked = $form_state->getValue(['enabled_blocked_users']);
    $enable_loggedin = $form_state->getValue(['enabled_loggedin_users']);
    $enable_never_loggedin = $form_state->getValue(['enabled_never_loggedin_users']);
    $enable_inactive = $form_state->getValue(['enabled_inactive_users']);
    $disregard_blocked = $form_state->getValue(['disregard_blocked_users']);

    // Validate to set purge period more than 10 days.
    if ($login_never_period == 'days' && !empty($login_never_value) && $login_never_value < 1 || $login_period == 'days' && !empty($login_value) && $login_value < 1 || $inactive_period == 'days' && !empty($inactive_value) && $inactive_value < 1 || $block_period == 'days' && !empty($block_value) && $block_value < 1 || $user_before_notification_period == 'days' && !empty($user_before_notification_value) && $user_before_notification_value < 1) {
      $form_state->setErrorByName('Period limit', $this->t('Purge period should be more than 10 days.'));
    }
    // Make sure one of the fieldset is checked.
    if (!$enable_loggedin && !$enable_inactive && !$enable_blocked && !$enable_never_loggedin) {
      $form_state->setErrorByName('Enable fieldset', $this->t('Please enable one of the Conditions:  Never logged in users, Not logged in users, Inactive users or Blocked users.'));
    }
    // Check if we don't both enable blocked users and disregard them.
    if ($enable_blocked && $disregard_blocked) {
      $form_state->setErrorByName('disregard_blocked_users', $this->t('You can not both purge blocked users and disregard them.'));
    }
    // Check if the don't both check for inactive users and disregard them.
    if ($enable_inactive && $disregard_blocked) {
      $form_state->setErrorByName('disregard_blocked_users', $this->t('You can not both purge inactive users and disregard them.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->setConfigFromForm($form_state);
    if ($form_state->getValue('purge_on_cron') == 1) {
      $this->messenger()
        ->addMessage($this->t('Purge users operation is scheduled for next cron.'));
    }
    if ($form_state->getValue('send_email_user_before_notification') == 1) {
      $this->messenger()->addMessage($this->t('Notification before purging users is enabled.'));
    }
  }

  /**
   * Submit handler for mass-account cancellation form.
   *
   * @see purge_users_config_form()
   */
  public function purgeUsersNowSubmit($form, FormStateInterface $form_state) {
    // Save form submissions.
    $this->setConfigFromForm($form_state);
    // If there is any user to purge.
    $ids_to_purge = purge_users_get_user_ids();
    $ids_to_notify = purge_users_get_user_ids('notification_users');
    if (!$ids_to_purge && !$ids_to_notify) {
      $this->messenger()->addMessage($this->t(
        'No user account found in the system to purge or notify.'));
      return;
    }

    $form_state->setRedirect('purge_users.confirm');
  }

  /**
   * Apply the form values to the config.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form to get the settings from.
   */
  protected function setConfigFromForm(FormStateInterface $form_state) {
    $config = $this->config('purge_users.settings');
    $config->set('user_never_lastlogin_value', $form_state->getValue('user_never_lastlogin_value'))
      ->set('user_never_lastlogin_period', $form_state->getValue('user_never_lastlogin_period'))
      ->set('user_lastlogin_value', $form_state->getValue('user_lastlogin_value'))
      ->set('user_lastlogin_period', $form_state->getValue('user_lastlogin_period'))
      ->set('user_inactive_value', $form_state->getValue('user_inactive_value'))
      ->set('user_inactive_period', $form_state->getValue('user_inactive_period'))
      ->set('user_blocked_value', $form_state->getValue('user_blocked_value'))
      ->set('user_blocked_period', $form_state->getValue('user_blocked_period'))
      ->set('enabled_never_loggedin_users', $form_state->getValue('enabled_never_loggedin_users'))
      ->set('enabled_loggedin_users', $form_state->getValue('enabled_loggedin_users'))
      ->set('enabled_inactive_users', $form_state->getValue('enabled_inactive_users'))
      ->set('enabled_blocked_users', $form_state->getValue('enabled_blocked_users'))
      ->set('purge_included_users_roles', $form_state->getValue('purge_included_users_roles'))
      ->set('purge_excluded_users_roles', $form_state->getValue('purge_excluded_users_roles'))
      ->set('purge_on_cron', $form_state->getValue('purge_on_cron'))
      ->set('inactive_user_notify_subject', $form_state->getValue('inactive_user_notify_subject'))
      ->set('inactive_user_notify_text', $form_state->getValue('inactive_user_notify_text'))
      ->set('send_email_notification', $form_state->getValue('send_email_notification'))
      ->set('purge_user_cancel_method', $form_state->getValue('purge_user_cancel_method'))
      ->set('disregard_blocked_users', $form_state->getValue('disregard_blocked_users'))
      ->set('user_before_deletion_subject', $form_state->getValue('user_before_deletion_subject'))
      ->set('user_before_deletion_text', $form_state->getValue('user_before_deletion_text'))
      ->set('send_email_user_before_notification', $form_state->getValue('send_email_user_before_notification'))
      ->set('user_before_notification_value', $form_state->getValue('user_before_notification_value'))
      ->set('user_before_notification_period', $form_state->getValue('user_before_notification_period'))
      ->save();
  }

}
